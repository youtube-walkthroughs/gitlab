# YouTube Walkthroughs for GitLab

This is where I place Word Documents for easy download by users of my Youtube videos that would like a quick and easy set of instructions in text to follow.

## Documents 
| Title                                 | Upload Date          |
| ------------------------------------- | -------------------- |
| Install Gitlab on RHEL8 variants.docx | 2021-08-28  0003 EDT |
